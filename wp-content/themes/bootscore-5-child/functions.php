<?php

	// style and scripts
	 add_action( 'wp_enqueue_scripts', 'bootscore_5_child_enqueue_styles' );
	 function bootscore_5_child_enqueue_styles() {
         
        // style.css
        wp_enqueue_style( 'owl-style', get_stylesheet_directory_uri() . '/script/assets/owl.carousel.min.css' ); 
        wp_enqueue_style( 'owl-theme', get_stylesheet_directory_uri() . '/script/assets/owl.theme.default.min.css' ); 
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
         
        // custom.js
        wp_enqueue_script('icons-js', 'https://unpkg.com/feather-icons', false, '', true);
        wp_enqueue_script('owl-js', get_stylesheet_directory_uri().'/script/owl.carousel.min.js', false, '', true);
        wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true);

     } 
