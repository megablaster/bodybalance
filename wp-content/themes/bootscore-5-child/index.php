<?php get_header();?>
<?php echo do_shortcode('[rev_slider alias="slider-1"][/rev_slider]');?>

<section id="info1">
	<div class="container">
		<div class="row">
			<div class="col-xl-6">
				<h2>Llego la hora <span>de cambiar tu vida</span></h2>
				<p>Descubre las bondades de Body Balance gracias a la ciencia innovadora de nuestros productos.</p>
				<p>Con un nuevo enfoque de emprendimiento, formando el balance perfecto para transformar la vida física, emocional y financiera tanto de clientes como de integrantes de la empresa.</p>
			</div>
			<div class="col-xl-5 offset-xl-1 p-relative">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/bg-img.jpg';?>" class="img-fluid bg">
			</div>
		</div>
	</div>
</section>
<section id="slide">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">

				<div class="owl-carousel owl-theme owl-slide">
					<div class="item">
						<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/slide-1.jpg';?>');">
						</div>
						<div class="info">
							<h3>Reduce el colesterol</h3>
							<p>Reduce el colesterol "malo", previene trastornos digestivos y regula el movimiento intestinal.</p>
						</div>
					</div>
					<div class="item">
						<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/slide-2.jpg';?>');">
						</div>
						<div class="info">
							<h3>Elimina la retención de líquidos</h3>
							<p>Con extractos de alisma eliminas la retención de líquidos y reduces el apetito.</p>
						</div>
					</div>
					<div class="item">
						<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/slide-3.jpg';?>');">
						</div>
						<div class="info">
							<h3>Reducción de depósitos de azúcar y grasas</h3>
							<p>Baja ingesta de calorías, impide la conversión de carbohidratos y azúcares en grasa, por tanto, reducir los depósitos de los mismos.</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<section id="products">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="scroll-slow" style="margin-bottom: 35px;">
					<h3>Nos vemos bien y sabemos el poder que tenemos… somos lo que somos porque eso es lo que queremos ser</h3>
				</div>
			</div>
			<div class="col-xl-5 offset-xl-1 col-lg-6">
				<div class="text">
					<h2>Conoce nuestros <span>productos</span></h2>
				    <p>Nuestros suplementos alimenticios naturales funcionan para bajar de peso y complementamos nuestras opciones con tés, proteínas, multivitamínicos, gel y fajas para que te sientas bien.</p>
				    <ul>
					   <li>
						  <a href="#" data-id="0" class="click-product active">High intensity</a>
					   </li>
					   <li>
						   <a href="#" class="click-product" data-id="1">Emergency boutique</a>
					   </li>
					   <li>
						   <a href="#" class="click-product" data-id="2">Fat burner man</a>
					   </li>
					   <li>
						   <a href="#" class="click-product" data-id="3">7 like</a>
					   </li>
				   </ul>
				   <a href="#form" class="more-information">Más información <i data-feather="chevron-right"></i></a>
				</div>
			</div>
			<div class="col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-6 offset-md-3 p-0">

				<div class="owl-carousel owl-theme owl-product">

					<div class="item">
						<div class="float">
						  <img src="<?php echo get_stylesheet_directory_uri().'/img/product-1.jpg';?>" class="img-fluid">
						  <div class="info pink">
						  	<h3>High Intensity</h3>
						  	<p>Gracias a su potente formulación, High Intensity te ofrece diferentes beneficios como la reducción de colesterol "malo",  pérdida de grasa, aumento de energía y líbido además de combatir el estreñimiento.</p>
						  	<a href="#" class="btnn">
						  		<i class="fas fa-chevron-right fa-3x"></i>
						  	</a>
						  	<img src="<?php echo get_stylesheet_directory_uri().'/img/medical-1.png';?>" class="medical">
						  </div>
						</div>
					</div>

					<div class="item">
						<div class="float">
						  <img src="<?php echo get_stylesheet_directory_uri().'/img/product-2.jpg';?>" class="img-fluid">
						  <div class="info green">
						  	<h3>EMERGENCY BOUTIQUE</h3>
						  	<p>Ideal para hombres y mujeres que están en el proceso de bajar de peso. Es un suplemento herbolario anti obesidad actúa como un balón gástrico que controla el apetito y provoca saciedad.</p>
						  	<a href="#" class="btnn">
						  		<i class="fas fa-chevron-right fa-3x"></i>
						  	</a>
						  	<img src="<?php echo get_stylesheet_directory_uri().'/img/medical-2.png';?>" class="medical">
						  </div>
						</div>
					</div>

					<div class="item">
						<div class="float">
						  <img src="<?php echo get_stylesheet_directory_uri().'/img/product-3.jpg';?>" class="img-fluid">
						  <div class="info blue">
						  	<h3>FAT BURNER MAN</h3>
						  	<p>Con su fórmula exclusiva ofrece un efecto termogénico que acelera el metabolismo, regenera masa muscular y controla el apetito mejorando la digestión incrementando la energía, el libido y vitalidad.</p>
						  	<a href="#" class="btnn">
						  		<i class="fas fa-chevron-right fa-3x"></i>
						  	</a>
						  	<img src="<?php echo get_stylesheet_directory_uri().'/img/medical-3.png';?>" class="medical">
						  </div>
						</div>
					</div>

					<div class="item">
						<div class="float">
						  <img src="<?php echo get_stylesheet_directory_uri().'/img/product-4.jpg';?>" class="img-fluid">
						  <div class="info purple">
						  	<h3>7 like factor</h3>
						  	<p>Con su fórmula exclusiva ofrece un efecto termogénico que acelera el metabolismo, regenera masa muscular y controla el apetito mejorando la digestión incrementando la energía, el libido y vitalidad.</p>
						  	<a href="#" class="btnn">
						  		<i class="fas fa-chevron-right fa-3x"></i>
						  	</a>
						  	<img src="<?php echo get_stylesheet_directory_uri().'/img/medical-4.png';?>" class="medical">
						  </div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</section>
<section id="distribuition">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p-0">
				<a href="#form">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/distribuidor.jpg';?>" class="img-fluid">
				</a>
			</div>
		</div>
	</div>
	<ul class="social">
		<li>
			<a href="#">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/instagram.png';?>" alt="Facebook">
			</a>
		</li>
		<li>
			<a href="#">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/facebook.png';?>" alt="Facebook">
			</a>
		</li>
		<li>
			<a href="#">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/twitter.png';?>" alt="Facebook">
			</a>
		</li>
		<li>
			<a href="#">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/whatsapp.png';?>" alt="Facebook">
			</a>
		</li>
	</ul>
</section>
<section id="use">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h2>Ellos ya descubrieron los beneficios de usar nuestros productos</h2>
			</div>
			<div class="col-xl-10 offset-xl-1 col-md-8 offset-md-2">

				<div class="owl-carousel owl-theme owl-use">
					<div class="item body">
						<div class="row">
							<div class="col-xl-5 col-lg-5 col-md-12 p-relative">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/use-1.jpg';?>" class="img-fluid cover">
							</div>
							<div class="col-xl-5 offset-xl-1 col-lg-5 offset-lg-1">
								<div class="text">
									<h3>"Una vez que emepecé a utilizar los productos body balance, mi vida cambió para siempre"</h3>
									<p>Guadalupe López</p>
									<strong>Infuencer Fitness</strong>
									<ul class="social">
										<li>
											<a href="#">
												<img src="<?php echo get_stylesheet_directory_uri().'/img/insta.png';?>" alt="Facebook">
											</a>
										</li>
										<li>
											<a href="#">
												<img src="<?php echo get_stylesheet_directory_uri().'/img/face.png';?>" alt="Facebook">
											</a>
										</li>
										<li>
											<a href="#">
												<img src="<?php echo get_stylesheet_directory_uri().'/img/twi.png';?>" alt="Facebook">
											</a>
										</li>
										<li>
											<a href="#">
												<img src="<?php echo get_stylesheet_directory_uri().'/img/what.png';?>" alt="Facebook">
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<section id="logo">
	<div class="scroll">
		<h2>NUESTROS PRODUCTOS CUENTAN CON CERTIFICACIONES DEL TIPO</h2>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xl-8 offset-xl-2">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/logos.png';?>" class="img-fluid">
			</div>
		</div>
	</div>
	<div class="scroll-slow">
		<h2>Y ESTUDIOS CIENTÍFICOS EN. Y ESTUDIOS CIENTÍFICOS EN.</h2>
	</div>
</section>
<section id="benefities">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-md-10 offset-md-1 col-10 offset-1 p-0 p-relative">

				<img src="<?php echo get_stylesheet_directory_uri().'/img/certificate.png';?>" class="img-fluid certificate">

				<div class="owl-carousel owl-theme owl-benefities">
					<div class="item">
						<div class="info">
							<h2>LABORATORIOS DE LA UNAM CERTIFICAN EL <span>PROCESO DE  LOS PRODUCTOS BODY BALANCE</span></h2>
						</div>
					</div>
					<div class="item">
						<div class="info">
							<h2>LABORATORIOS DE LA UNAM CERTIFICAN EL <span>PROCESO DE  LOS PRODUCTOS BODY BALANCE</span></h2>
						</div>
					</div>
					<div class="item">
						<div class="info">
							<h2>LABORATORIOS DE LA UNAM CERTIFICAN EL <span>PROCESO DE  LOS PRODUCTOS BODY BALANCE</span></h2>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<section id="blank"></section>
<section id="form">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h2 class="text-center">¿algo en lo que te podamos ayudar?</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6">
				<h3>Quisiera tener más información sobre:</h3>
				<ul class="list">
					<li>
						<input type="radio" name="info" id="productos" value="productos">
						<label for="productos">Productos</label><br>
					</li>
					<li>
						<input type="radio" name="info" id="beneficios" value="beneficios">
						<label for="beneficios">Beneficios</label><br>
					</li>
					<li>
						<input type="radio" name="info" id="distribuidor" value="distribuidor">
						<label for="distribuidor">Ser distribuidor</label><br>
					</li>
					<li>
						<input type="radio" name="info" id="certificados" value="certificados">
						<label for="certificados">Certificados</label><br>
					</li>
					<li>
						<input type="radio" name="info" id="estudios" value="estudios">
						<label for="estudios">Estudios</label><br>
					</li>
				</ul>
			</div>
			<div class="col-xl-5">
				<h3>¿Tienes alguna otra duda? ¡contáctanos!</h3>
				<div class="form">

					<?php echo do_shortcode('[contact-form-7 id="21" title="Contacto"]');?>

				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer();?>