jQuery(document).ready(function ($) {

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        if (scroll >= 290) {
            $(".circle").addClass("fixed");
        } else {
            $(".circle").removeClass("fixed");
        }
    });

    $('#menu-click').on('click', function(){
        $('.off-canvas').toggleClass('slide');
        $('#grey').toggleClass('active');
    });

    $('#close-menu').on('click', function(){
        $('.off-canvas').toggleClass('slide');
        $('#grey').toggleClass('active');
    });

    $('#grey').on('click', function(){
        $('.off-canvas').toggleClass('slide');
        $('#grey').toggleClass('active');
    });

    $('.main a,.sub-main a').on('click', function(){
        $('.off-canvas').toggleClass('slide');
        $('#grey').toggleClass('active');
    });

    $('.owl-slide').owlCarousel({
        loop:true,
        margin:50,
        nav:false,
        center:true,
        autoplay:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                margin:0,
                items:1
            },
            768:{
                margin:0,
                center:false,
                items:1
            },
            992: {
                margin:0,
                items:2
            },
            1366:{
                margin:20,
                items:2
            },
            1536:{
                items:3
            },
        }
    });

    $('.owl-benefities').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        autoplay:true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.owl-use').owlCarousel({
        loop:true,
        nav:true,
        dots:true,
        autoplay:true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    //Productos
    var product = $('.owl-product');
    product.owlCarousel({
        loop:true,
        margin:0,
        dots:false,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.btnn').click(function(e) {
        e.preventDefault();
        product.trigger('next.owl.carousel');
    })

    $('.click-product').on('click', function(e){
        e.preventDefault();
        var id = $(this).data('id');

        $('.click-product').removeClass('active');
        $(this).addClass('active');

        //Change product
        product.trigger('to.owl.carousel', [id,300]);
        
    });

}); // jQuery End
