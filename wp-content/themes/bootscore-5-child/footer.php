<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootscore
 */

?>

            <div class="top-button position-fixed zi-1020">
                <a href="#to-top" class="btn btn-primary shadow"><i class="fas fa-chevron-up"></i></a>
            </div>

        </div><!-- #page -->

        <?php wp_footer(); ?>

        <script>
          feather.replace()
        </script>

    </body>
</html>
