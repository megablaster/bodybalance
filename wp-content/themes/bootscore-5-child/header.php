<?php
	/**
	 * The header for our theme
	 *
	 * This is the template that displays all of the <head> section and everything up until <div id="content">
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package Bootscore
	 */
	
	?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <!-- Favicons -->
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/safari-pinned-tab.svg" color="#0d6efd">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Loads the internal WP jQuery. Required if a 3rd party plugin loads jQuery in header instead in footer -->
    <?php wp_enqueue_script('jquery'); ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="to-top"></div>

    <div id="page" class="site">

        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xl-3">
                        <div class="circle">
                            <i data-feather="menu" id="menu-click"></i>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="off-canvas">
            <i class="fas fa-times" id="close-menu"></i>
            <ul class="main">
                <li><a href="#info1">Acerca de</a></li>
                <li><a href="#products">Productos</a></li>
                <li><a href="#slide">Beneficios</a></li>
                <li><a href="#benefities">Estudios</a></li>
                <li><a href="#logo">Certificados</a></li>
                <li><a href="#form">¡Quiero ser distribuidor!</a></li>
            </ul>
            <ul class="sub-main">
                <li><a href="#form">Contacto</a></li>
                <li><a href="#use">Testimonios</a></li>
                <li><a href="#form">Distribuidores oficiales</a></li>
            </ul>
            <img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logo-white.png';?>" class="img-fluid logo">
            <ul class="social">
                <li><a href="#"></a></li>
            </ul>
        </div>

        <div id="grey"></div>

        <?php bootscore_ie_alert(); ?>
